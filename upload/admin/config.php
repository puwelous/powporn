<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/openCart/upload/admin/');
define('HTTP_CATALOG', 'http://localhost/openCart/upload/');
define('HTTP_IMAGE', 'http://localhost/openCart/upload/image/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/openCart/upload/admin/');
define('HTTPS_IMAGE', 'http://localhost/openCart/upload/image/');

// DIR
define('DIR_APPLICATION', '/home/pali/public_html/openCart/upload/admin/');
define('DIR_SYSTEM', '/home/pali/public_html/openCart/upload/system/');
define('DIR_DATABASE', '/home/pali/public_html/openCart/upload/system/database/');
define('DIR_LANGUAGE', '/home/pali/public_html/openCart/upload/admin/language/');
define('DIR_TEMPLATE', '/home/pali/public_html/openCart/upload/admin/view/template/');
define('DIR_CONFIG', '/home/pali/public_html/openCart/upload/system/config/');
define('DIR_IMAGE', '/home/pali/public_html/openCart/upload/image/');
define('DIR_CACHE', '/home/pali/public_html/openCart/upload/system/cache/');
define('DIR_DOWNLOAD', '/home/pali/public_html/openCart/upload/download/');
define('DIR_LOGS', '/home/pali/public_html/openCart/upload/system/logs/');
define('DIR_CATALOG', '/home/pali/public_html/openCart/upload/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'pali');
define('DB_PASSWORD', 'admin');
define('DB_DATABASE', 'opencart');
define('DB_PREFIX', 'oc_');
?>